#[cfg(feature = "alloc")]
use alloc::string::String;
use core::{
    any::type_name,
    fmt::Display,
    marker::PhantomData,
    num::{ParseFloatError, ParseIntError},
};
#[derive(Debug, PartialEq, Eq)]
pub enum StringParseFailed<D> {
    Unknown(PhantomData<D>),
    #[cfg(not(any(feature = "std", feature = "alloc")))]
    Bool,
    #[cfg(any(feature = "std", feature = "alloc"))]
    Bool(String),
    Int(ParseIntError),
    Float(ParseFloatError),
}
impl<D> Display for StringParseFailed<D> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            StringParseFailed::Unknown(_) => f.write_fmt(format_args!(
                "Failed to parse string into type {}; Reason unknown !",
                type_name::<D>()
            )),
            #[cfg(any(feature = "std", feature = "alloc"))]
            StringParseFailed::Bool(value) => {
                f.write_fmt(format_args!("Failed to parse '{}' to boolean", value))
            }
            #[cfg(not(any(feature = "std", feature = "alloc")))]
            StringParseFailed::Bool => f.write_str("Failed to parse a string value into boolean"),
            StringParseFailed::Int(source) => f.write_fmt(format_args!("{}", source)),
            StringParseFailed::Float(source) => f.write_fmt(format_args!("{}", source)),
        }
    }
}
