use crate::{Infallible, LosslessFrom, StringParseFailed};
#[cfg(any(feature = "std", feature = "alloc"))]
// impl_bool
impl LosslessFrom<&str> for bool {
    type TError = StringParseFailed<bool>;
    fn lossless_from(v: &str) -> Result<Self, Self::TError> {
        let l = v.to_lowercase();
        match l.as_ref() {
            "1" | "t" | "y" | "true" | "yes" => Ok(true),
            "0" | "f" | "n" | "false" | "no" => Ok(false),
            _ => v.parse().map_err(|_| Self::TError::Bool(l)),
        }
    }
}
impl const LosslessFrom<bool> for &'static str {
    type TError = Infallible;
    fn lossless_from(v: bool) -> Result<Self, Self::TError> {
        if v {
            Ok("true")
        } else {
            Ok("false")
        }
    }
}
impl LosslessFrom<bool> for String {
    type TError = Infallible;
    fn lossless_from(v: bool) -> Result<Self, Self::TError> {
        if v {
            Ok(String::from("true"))
        } else {
            Ok(String::from("false"))
        }
    }
}
#[cfg(test)]
mod tests {
    use super::*;
    use crate::LosslessInto;
    #[test]
    #[cfg(any(feature = "std", feature = "alloc"))]
    fn test_str_to_bool() {
        assert_eq!(Ok(true), bool::lossless_from("1"));
        assert_eq!(Ok(true), bool::lossless_from("t"));
        assert_eq!(Ok(true), bool::lossless_from("T"));
        assert_eq!(Ok(true), bool::lossless_from("y"));
        assert_eq!(Ok(true), bool::lossless_from("Y"));
        assert_eq!(Ok(true), bool::lossless_from("yes"));
        assert_eq!(Ok(true), bool::lossless_from("YeS"));
        assert_eq!(Ok(true), bool::lossless_from("true"));
        assert_eq!(Ok(true), bool::lossless_from("tRUe"));
        assert_eq!(Ok(false), bool::lossless_from("0"));
        assert_eq!(Ok(false), bool::lossless_from("f"));
        assert_eq!(Ok(false), bool::lossless_from("F"));
        assert_eq!(Ok(false), bool::lossless_from("n"));
        assert_eq!(Ok(false), bool::lossless_from("N"));
        assert_eq!(Ok(false), bool::lossless_from("no"));
        assert_eq!(Ok(false), bool::lossless_from("nO"));
        assert_eq!(Ok(false), bool::lossless_from("FalSe"));
    }
    #[test]
    fn test_bool_to_str() {
        assert_eq!(Ok("true"), true.lossless_into());
        assert_eq!(Ok("false"), false.lossless_into());
        assert_eq!(Ok("true"), true.lossless_into());
        assert_eq!(Ok("false"), false.lossless_into());
    }
}
