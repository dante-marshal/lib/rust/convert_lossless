use crate::{Infallible, LosslessFrom, StringParseFailed};
macro_rules! impl_from_string {
    ($E: ident, $($D: ident),+) => {$(
        impl LosslessFrom<&str> for $D where {
            type TError = StringParseFailed<$D>;
            fn lossless_from(v: &str) -> Result<Self, Self::TError> {
                v.parse().map_err(Self::TError::$E)
            }
        }
        impl LosslessFrom<String> for $D where {
            type TError = StringParseFailed<$D>;
            fn lossless_from(v: String) -> Result<Self, Self::TError> {
                v.parse().map_err(Self::TError::$E)
            }
        }
        paste::paste!{
            #[test]
            #[cfg(test)]
            fn [<test_ str _to_ $D>]() {
                let v = $D::lossless_from("127");
                assert_eq!(Ok(127 as $D), v);
                let v = $D::lossless_from("126".to_owned());
                assert_eq!(Ok(126 as $D), v);
            }
        }
    )+};
}
#[cfg(any(feature = "std", feature = "alloc"))]
macro_rules! impl_to_string {
    ($($S: ident),+) => {$(
        impl LosslessFrom<$S> for String where {
            type TError = Infallible;
            fn lossless_from(v: $S) -> Result<Self, Self::TError> {
                Ok(v.to_string())
            }
        }
        paste::paste!{
            #[test]
            #[cfg(test)]
            fn [<test_ $S _to_ str>]() {
                assert_eq!(Ok(String::from("127")), String::lossless_from(127));
            }
        }
    )+};
}
impl_from_string!(Int, i8, i16, i32, i64, i128);
impl_from_string!(Int, u8, u16, u32, u64, u128);
impl_from_string!(Float, f32, f64);
#[cfg(any(feature = "std", feature = "alloc"))]
impl_to_string!(i8, i16, i32, i64, i128);
#[cfg(any(feature = "std", feature = "alloc"))]
impl_to_string!(u8, u16, u32, u64, u128);
#[cfg(any(feature = "std", feature = "alloc"))]
impl_to_string!(f32, f64);
