#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct CountMismatchError {
    pub expected: usize,
    pub actual: usize
}
