use crate::{markers::LosslessSingleItem, CountMismatchError, LosslessFrom};
mod markers {
    use crate::markers::LosslessSingleItem;
    impl LosslessSingleItem for bool {}
    impl LosslessSingleItem for char {}
    impl LosslessSingleItem for f32 {}
    impl LosslessSingleItem for f64 {}
    impl LosslessSingleItem for i8 {}
    impl LosslessSingleItem for i16 {}
    impl LosslessSingleItem for i32 {}
    impl LosslessSingleItem for i64 {}
    impl LosslessSingleItem for i128 {}
    impl LosslessSingleItem for u8 {}
    impl LosslessSingleItem for u16 {}
    impl LosslessSingleItem for u32 {}
    impl LosslessSingleItem for u64 {}
    impl LosslessSingleItem for u128 {}
    impl LosslessSingleItem for String {}
    impl LosslessSingleItem for &str {}
}
impl<T: LosslessSingleItem> LosslessFrom<Vec<T>> for T {
    type TError = CountMismatchError;
    fn lossless_from(v: Vec<T>) -> Result<Self, Self::TError> {
        let mut v = v;
        let len = v.len();
        if len == 1 {
            Ok(v.remove(0))
        } else {
            Err(CountMismatchError {
                expected: 1,
                actual: len,
            })
        }
    }
}
