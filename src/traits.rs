pub trait LosslessFrom<T>: Sized {
    type TError;
    fn lossless_from(v: T) -> Result<Self, Self::TError>;
}
pub trait LosslessInto<T> {
    type TError;
    fn lossless_into(self) -> Result<T, Self::TError>;
}
/// Blanket impl LosslessInto for LosslessFrom
impl<TSrc, TDst> LosslessInto<TDst> for TSrc
where
    TDst: LosslessFrom<TSrc>,
{
    type TError = <TDst as LosslessFrom<TSrc>>::TError;
    fn lossless_into(self) -> Result<TDst, Self::TError> {
        TDst::lossless_from(self)
    }
}
/// Blanket impl LosslessFrom for a type to itself
impl<T> LosslessFrom<T> for T {
    type TError = crate::Infallible;
    fn lossless_from(v: Self) -> Result<Self, Self::TError> {
        Ok(v)
    }
}
