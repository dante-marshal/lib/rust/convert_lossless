use crate::{BoundReached, LosslessFrom};
macro_rules! impl_bounded {
    ($S: ident; $($D: ident),+; $V: expr; $L: expr; $U: expr) => {$(
        impl LosslessFrom<$S> for $D {
            type TError = BoundReached<$S, $D>;
            fn lossless_from(v: $S) -> Result<Self, Self::TError> {
                if(v < ($D::MIN as $S)){
                    Err(Self::TError::lower(v, $D::MIN))
                } else if(v > ($D::MAX as $S)) {
                    Err(Self::TError::upper(v, $D::MAX))
                } else {
                    Ok(v as $D)
                }
            }
        }
        paste::paste!{
            #[test]
            #[cfg(test)]
            fn [<test_ $S _to_ $D>]() {
                for n in $V{
                    assert_eq!(Ok(n as $D), $D::lossless_from(n as $S));
                }
                for n in $L{
                    assert_eq!(Err(BoundReached::<$S, $D>::lower(n, $D::MIN)), $D::lossless_from(n as $S));
                }
                for n in $U{
                    assert_eq!(Err(BoundReached::<$S, $D>::upper(n, $D::MAX)), $D::lossless_from(n as $S));
                }
            }
        }
    )+};
}
impl_bounded!(i16; i8, u8; [0, i8::MAX]; [i16::MIN]; [i16::MAX]);
impl_bounded!(i32; i8, i16, u8, u16; [0, i8::MAX]; [i32::MIN]; [i32::MAX]);
impl_bounded!(i64; i8, i16, i32, u8, u16, u32; [0, i8::MAX]; [i64::MIN]; [i64::MAX]);
impl_bounded!(i128; i8, i16, i32, i64, u8, u16, u32, u64; [0, i8::MAX]; [i128::MIN]; [i128::MAX]);
