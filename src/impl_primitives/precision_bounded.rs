#[cfg(feature = "std")]
mod impl_float_to_int {
    use crate::{BoundReached, LosslessFrom};
    use core::num::FpCategory;
    macro_rules! impl_precision_float_to_int {
        ($S: ident; $($D: ident),+; $V: expr, $P: expr, $L: expr, $U: expr) => {$(
            impl LosslessFrom<$S> for $D {
                type TError = BoundReached<$S, $D>;
                fn lossless_from(v: $S) -> Result<Self, Self::TError> {
                    match v.classify() {
                        FpCategory::Nan => Err(BoundReached::precision(v, v as $D)),
                        FpCategory::Infinite => Err(if v.is_sign_negative() { BoundReached::lower(v, $D::MIN) } else { BoundReached::upper(v, $D::MAX) }),
                        FpCategory::Zero => {
                            if v.is_sign_negative() {
                                Err(BoundReached::precision(v, 0 as $D))
                            } else {
                                Ok(0 as $D)
                            }
                        }
                        FpCategory::Subnormal => Err(BoundReached::precision(v, 0 as $D)),
                        FpCategory::Normal => {
                            if v.fract().abs() > 0.0 {
                                Err(BoundReached::precision(v, v as $D))
                            } else {
                                Ok(v as $D)
                            }
                        }
                    }
                }
            }
            paste::paste!{
                #[test]
                #[cfg(test)]
                fn [<test_ $S _to_ $D>]() {
                    for n in $V{
                        assert_eq!(Ok(n as $D), $D::lossless_from($S::from(n)));
                    }
                    for n in $P{
                        if $S::is_nan(n) {
                            if let BoundReached::Precision(err) = $D::lossless_from($S::from(n)).unwrap_err() {
                                assert!($S::is_nan(err.src));
                                assert_eq!(err.best_match, 0 as $D);
                            } else {
                                panic!("Invalid error variant returned !");
                            }
                        } else {
                            assert_eq!(Err(BoundReached::precision(n, n as $D)), $D::lossless_from($S::from(n)));
                        }
                    }
                    for n in $L{
                        assert_eq!(Err(BoundReached::lower(n, $D::MIN)), $D::lossless_from($S::from(n)));
                    }
                    for n in $U{
                        assert_eq!(Err(BoundReached::upper(n, $D::MAX)), $D::lossless_from($S::from(n)));
                    }
                }
            }
        )+};
    }
    impl_precision_float_to_int!(f32; i8, i16, i32, i64, i128, u8, u16, u32, u64, u128; [0f32, i8::MAX.into()], [-0f32, f32::NAN, f32::EPSILON], [f32::NEG_INFINITY], [f32::INFINITY]);
    impl_precision_float_to_int!(f64; i8, i16, i32, i64, i128, u8, u16, u32, u64, u128; [0f64, i8::MAX.into()], [-0f64, f64::NAN, f64::EPSILON], [f64::NEG_INFINITY], [f64::INFINITY]);
}
#[cfg(not(feature = "std"))]
mod impl_float_to_int {
    use super::super::{BoundReached, LosslessFrom};
    use core::num::FpCategory;
    // Under FpCategory::Normal match arm, `v - (r as $S)` might cause problems in the future
    macro_rules! impl_precision_float_to_int {
        ($S: ident; $($D: ident),+; $V: expr, $P: expr, $L: expr, $U: expr) => {$(
            impl LosslessFrom<$S> for $D {
                type TError = BoundReached<$S, $D>;
                fn lossless_from(v: $S) -> Result<Self, Self::TError> {
                    match v.classify() {
                        FpCategory::Nan => Err(BoundReached::precision(v, v as $D)),
                        FpCategory::Infinite => Err(if v.is_sign_negative() { BoundReached::lower(v, $D::MIN) } else { BoundReached::upper(v, $D::MAX) }),
                        FpCategory::Zero => {
                            if v.is_sign_negative() {
                                Err(BoundReached::precision(v, 0 as $D))
                            } else {
                                Ok(0 as $D)
                            }
                        }
                        FpCategory::Subnormal => Err(BoundReached::precision(v, 0 as $D)),
                        FpCategory::Normal => {
                            let r = v as $D;
                            if (v - (r as $S)) > 0.0 {
                                Err(BoundReached::precision(v, v as $D))
                            } else {
                                Ok(v as $D)
                            }
                        }
                    }
                }
            }
            paste::paste!{
                #[test]
                #[cfg(test)]
                fn [<test_ $S _to_ $D>]() {
                    for n in $V{
                        assert_eq!(Ok(n as $D), $D::lossless_from($S::from(n)));
                    }
                    for n in $P{
                        if $S::is_nan(n) {
                            if let BoundReached::Precision(err) = $D::lossless_from($S::from(n)).unwrap_err() {
                                assert!($S::is_nan(err.src));
                                assert_eq!(err.best_match, 0 as $D);
                            } else {
                                panic!("Invalid error variant returned !");
                            }
                        } else {
                            assert_eq!(Err(BoundReached::precision(n, n as $D)), $D::lossless_from($S::from(n)));
                        }
                    }
                    for n in $L{
                        assert_eq!(Err(BoundReached::lower(n, $D::MIN)), $D::lossless_from($S::from(n)));
                    }
                    for n in $U{
                        assert_eq!(Err(BoundReached::upper(n, $D::MAX)), $D::lossless_from($S::from(n)));
                    }
                }
            }
        )+};
    }
    impl_precision_float_to_int!(f32; i8, i16, i32, i64, i128, u8, u16, u32, u64, u128; [0f32, i8::MAX.into()], [-0f32, f32::NAN, f32::EPSILON], [f32::NEG_INFINITY], [f32::INFINITY]);
    impl_precision_float_to_int!(f64; i8, i16, i32, i64, i128, u8, u16, u32, u64, u128; [0f64, i8::MAX.into()], [-0f64, f64::NAN, f64::EPSILON], [f64::NEG_INFINITY], [f64::INFINITY]);
}
mod impl_int_to_float {
    use crate::{BoundReached, LosslessFrom};
    macro_rules! impl_precision_int_to_float {
        (tests $S: ident; $D: ident; $V: expr, $P: expr) => {
            paste::paste!{
                #[test]
                #[cfg(test)]
                #[cfg(feature = "std")]
                fn [<test_ $S _to_ $D _v>]() {
                    for n in $V{
                        let lhs = Ok(n as $D);
                        let rhs = $D::lossless_from(n as $S);
                        if lhs != rhs {
                            let lhs = lhs.unwrap().to_string();
                            let rhs = rhs.unwrap_err().to_string();
                            assert_eq!(lhs, rhs);
                        }
                    }
                }
                #[test]
                #[cfg(test)]
                #[cfg(feature = "std")]
                fn [<test_ $S _to_ $D _i>]() {
                    for n in $P{
                        let lhs = Err(BoundReached::<$S, $D>::precision(n, n as $D));
                        let rhs = $D::lossless_from(n as $S);
                        if lhs != rhs {
                            let lhs = lhs.unwrap_err().to_string();
                            let rhs = rhs.unwrap().to_string();
                            assert_eq!(lhs, rhs);
                        }
                    }
                }
            }
        };
        (u $($S: ident $V: expr, $P: expr),+; $D: ident) => {$(
            impl LosslessFrom<$S> for $D {
                type TError = BoundReached<$S, $D>;
                fn lossless_from(v: $S) -> Result<Self, Self::TError> {
                    let digits = $S::BITS.saturating_sub(v.leading_zeros() + v.trailing_zeros());
                    #[allow(clippy::cast_precision_loss)]
                    let d = v as $D;
                    if digits <= $D::MANTISSA_DIGITS && v == (d as $S) {
                        Ok(d)
                    } else {
                        Err(Self::TError::precision(v, d))
                    }
                }
            }
            impl_precision_int_to_float!(tests $S; $D; $V, $P);
        )+};
        (s $($S: ident $V: expr, $P: expr),+; $D: ident) => {$(
            impl LosslessFrom<$S> for $D {
                type TError = BoundReached<$S, $D>;
                fn lossless_from(v: $S) -> Result<Self, Self::TError> {
                    if v == $S::MIN || v == $S::MAX { return Err(Self::TError::precision(v, v as $D)); }
                    let v_pos = if v < 0 { -v } else { v };
                    let digits = $S::BITS.saturating_sub(v_pos.leading_zeros() + v_pos.trailing_zeros());
                    #[allow(clippy::cast_precision_loss)]
                    let d = v as $D;
                    if digits <= $D::MANTISSA_DIGITS && v == (d as $S) {
                        Ok(d)
                    } else {
                        Err(Self::TError::precision(v, d))
                    }
                }
            }
            impl_precision_int_to_float!(tests $S; $D; $V, $P);
        )+};
    }
    impl_precision_int_to_float!(s
        i32 [0i32, -16777202, -16777218, -16777216, 0, 16777216, 16777218], [i32::MIN, -16777219, -16777217, 16777217, 16777219, i32::MAX],
        i64 [0i64, -21474834], [i64::MIN, -214748340, i64::MAX],
        i128 [0i128, -21474834], [i128::MIN, -214748340, i128::MAX];
        f32
    );
    impl_precision_int_to_float!(u
        u32 [0u32, 16777216, 16777218], [16777217, 16777219, u32::MAX],
        u64 [0u64, 21474834], [u64::MAX, 214748340],
        u128 [0u128, 21474834], [u128::MAX, 214748340];
        f32
    );
    impl_precision_int_to_float!(s
        i64 [0i64, 2147483499903001, -2147483499903001], [i64::MIN, -21474834999030010, i64::MAX],
        i128 [0i128, 2147483499903001, -2147483499903001], [i128::MIN, -21474834999030010, i128::MAX];
        f64
    );
    impl_precision_int_to_float!(u
        u64 [0u64, 2147483499903001], [u64::MAX, 21474834999030010],
        u128 [0u128, 2147483499903001], [u128::MAX, 21474834999030010];
        f64
    );
}
mod impl_float_to_float {
    use crate::{LosslessFrom, PrecisionBoundReached};
    impl LosslessFrom<f64> for f32 {
        type TError = PrecisionBoundReached<f64, f32>;
        fn lossless_from(v: f64) -> Result<Self, Self::TError> {
            if v.is_nan() {
                Ok(f32::NAN)
            } else {
                #[allow(clippy::cast_possible_truncation)]
                let r = v as f32;
                if v == f64::from(r) {
                    Ok(r)
                } else {
                    Err(PrecisionBoundReached::new(v, r))
                }
            }
        }
    }
    #[test]
    #[cfg(test)]
    #[allow(clippy::cast_possible_truncation)]
    fn test_f64_to_f32() {
        let valids = [
            0.0f64,
            0.5f64,
            2.0f64,
            f32::MAX.into(),
            f64::INFINITY,
            f64::NEG_INFINITY,
        ];
        let invalids = [0.2f64, 0.4f64];
        assert_eq!(Ok(true), f32::lossless_from(f64::NAN).map(f32::is_nan));
        for n in valids {
            assert_eq!(Ok(n as f32), f32::lossless_from(n));
        }
        for n in invalids {
            assert_eq!(
                Err(PrecisionBoundReached::new(n, n as f32)),
                f32::lossless_from(n)
            );
        }
    }
}
