use crate::{BooleanBoundReached, Infallible, LosslessFrom};

macro_rules! impl_into_boolean {
    (int $S: ident) => {
        impl const LosslessFrom<$S> for bool {
            type TError = BooleanBoundReached<$S, bool>;
            fn lossless_from(v: $S) -> Result<Self, Self::TError> {
                match v {
                    0 => Ok(false),
                    1 => Ok(true),
                    v => Err(BooleanBoundReached::new(v, v > 0))
                }
            }
        }
    };
    (f $($S: ident),+) => {$(
        #[cfg(feature = "std")]
        impl LosslessFrom<$S> for bool {
            type TError = BooleanBoundReached<$S, bool>;
            fn lossless_from(v: $S) -> Result<Self, Self::TError> {
                if($S::abs(v) < $S::EPSILON) {
                    Ok(false)
                } else if ($S::abs(v - 1.0) < $S::EPSILON) {
                    Ok(true)
                } else {
                    Err(BooleanBoundReached::new(v, v > 0.0))
                }
            }
        }
        #[cfg(not(feature = "std"))]
        impl LosslessFrom<$S> for bool {
            type TError = BooleanBoundReached<$S, bool>;
            fn lossless_from(v: $S) -> Result<Self, Self::TError> {
                let abs_v = if v < 0.0 {-v} else {v};
                if(abs_v < $S::EPSILON) {
                    Ok(false)
                } else if ((abs_v - 1.0) < $S::EPSILON) {
                    Ok(true)
                } else {
                    Err(BooleanBoundReached::new(v, v > 0.0))
                }
            }
        }
        paste::paste!{
            #[cfg(test)]
            #[test]
            fn [<test_ $S _to_bool>]() {
                assert_eq!(Ok(true), bool::lossless_from(1.0 as $S));
                assert_eq!(Ok(false), bool::lossless_from(0.0 as $S));
                assert_eq!(Err(BooleanBoundReached::<$S, bool>::new(2.0, true)), bool::lossless_from(2.0 as $S));
                assert_eq!(Err(BooleanBoundReached::<$S, bool>::new(-2.0, false)), bool::lossless_from(-2.0 as $S));
                assert_eq!(Err(BooleanBoundReached::<$S, bool>::new($S::INFINITY, true)), bool::lossless_from($S::INFINITY as $S));
                assert_eq!(Err(BooleanBoundReached::<$S, bool>::new($S::NEG_INFINITY, false)), bool::lossless_from($S::NEG_INFINITY as $S));
                let err = bool::lossless_from($S::NAN).unwrap_err();
                assert!($S::is_nan(err.src));
                assert_eq!(err.best_match, false);
            }
        }
    )+};
    (i $($S: ident),+) => {$(
        impl_into_boolean!(int $S);
        paste::paste!{
            #[cfg(test)]
            #[test]
            fn [<test_ $S _to_bool>]() {
                assert_eq!(Ok(true), bool::lossless_from(1 as $S));
                assert_eq!(Ok(false), bool::lossless_from(0 as $S));
                assert_eq!(Err(BooleanBoundReached::<$S, bool>::new(2, true)), bool::lossless_from(2 as $S));
            }
        }
    )+};
    (u $($S: ident),+) => {$(
        impl_into_boolean!(int $S);
        paste::paste!{
            #[cfg(test)]
            #[test]
            fn [<test_ $S _to_bool>]() {
                assert_eq!(Ok(true), bool::lossless_from(1 as $S));
                assert_eq!(Ok(false), bool::lossless_from(0 as $S));
                assert_eq!(Err(BooleanBoundReached::<$S, bool>::new(2, true)), bool::lossless_from(2 as $S));
            }
        }
    )+};
}
macro_rules! impl_from_boolean {
    ($($D: ident),+) => {$(
        impl const LosslessFrom<bool> for $D {
            type TError = Infallible;
            fn lossless_from(v: bool) -> Result<Self, Self::TError> {
                Ok(if v {(1 as $D)} else {(0 as $D)})
            }
        }
        paste::paste!{
            #[cfg(test)]
            #[test]
            fn [<test_bool_to_ $D>]() {
                assert_eq!(Ok(1 as $D), $D::lossless_from(true));
                assert_eq!(Ok(0 as $D), $D::lossless_from(false));
            }
        }
    )+};
}
impl_from_boolean!(f32, f64, i8, i16, i32, i64, i128, u8, u16, u32, u64, u128);
impl_into_boolean!(i i8, i16, i32, i64, i128, u8, u16, u32, u64, u128);
impl_into_boolean!(f f32, f64);
