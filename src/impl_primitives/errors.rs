pub use core::convert::Infallible;
#[cfg(feature = "error_in_core")]
use core::error::Error;
use core::{
    any::type_name,
    fmt::{Debug, Display, Formatter, Result as FmtResult}
};
#[cfg(any(feature = "std", feature = "error_in_core"))]
use std::error::Error;
macro_rules! impl_bound_error {
    ($TErr: ident, $MsgErr: literal) => {
        #[allow(clippy::derive_partial_eq_without_eq)]
        #[derive(Debug, PartialEq)]
        pub struct $TErr<S, D> {
            pub src: S,
            pub bound: D,
        }
        impl<S: PartialEq, D> $TErr<S, D> {
            pub const fn new(src: S, bound: D) -> Self {
                Self { src, bound }
            }
        }
        impl<S: PartialEq + Display, D: Display> Display for $TErr<S, D> {
            fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
                f.write_fmt(format_args!(
                    "{} : The source value '{}' {} '{}'",
                    type_name::<Self>(),
                    self.src,
                    $MsgErr,
                    self.bound
                ))
            }
        }
        #[cfg(any(feature = "std", feature = "error_in_core"))]
        impl<S: PartialEq + Display + Debug, D: Display + Debug> Error for $TErr<S, D> {}
    };
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Debug, PartialEq)]
pub struct PrecisionBoundReached<S, D> {
    pub src: S,
    pub best_match: D,
}
impl<S, D> PrecisionBoundReached<S, D> {
    pub const fn new(src: S, best_match: D) -> Self {
        Self { src, best_match }
    }
}
impl<S: Display, D: Display> Display for PrecisionBoundReached<S, D> {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        f.write_fmt(format_args!(
            "{} : The source value '{}' is out of supported precision of the destination type. Best match is {}",
            type_name::<Self>(),
            self.src,
            self.best_match
        ))
    }
}
#[cfg(any(feature = "std", feature = "error_in_core"))]
impl<S: Display + Debug, D: Display + Debug> Error for PrecisionBoundReached<S, D> {}
impl_bound_error!(
    LowerBoundReached,
    "is less than the least supported destination value"
);
impl_bound_error!(
    UpperBoundReached,
    "is greater than the greatest supported destination value"
);

#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Debug, PartialEq)]
pub struct BooleanBoundReached<S, D> {
    pub src: S,
    pub best_match: D,
}
impl<S, D> BooleanBoundReached<S, D> {
    pub const fn new(src: S, best_match: D) -> Self {
        Self { src, best_match }
    }
}
impl<S: Display, D: Display> Display for BooleanBoundReached<S, D> {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        f.write_fmt(format_args!(
            "{} : The source value '{}' is out of supported precision of the destination type (boolean). Expected something that correctly corresponds to `true` or `false` (e.g. exactly 0 or 1 for numeric types).",
            type_name::<Self>(),
            self.src,
        ))
    }
}
#[cfg(any(feature = "std", feature = "error_in_core"))]
impl<S: Display + Debug, D: Display + Debug> Error for BooleanBoundReached<S, D> {}

#[derive(Debug, PartialEq)]
pub enum BoundReached<S, D> {
    Lower(LowerBoundReached<S, D>),
    Upper(UpperBoundReached<S, D>),
    Precision(PrecisionBoundReached<S, D>),
    Boolean(BooleanBoundReached<S, D>),
}
impl<S: PartialEq, D> BoundReached<S, D> {
    pub const fn lower(src: S, bound: D) -> Self {
        Self::Lower(LowerBoundReached { src, bound })
    }
    pub const fn upper(src: S, bound: D) -> Self {
        Self::Upper(UpperBoundReached { src, bound })
    }
    pub const fn precision(src: S, best_match: D) -> Self {
        Self::Precision(PrecisionBoundReached { src, best_match })
    }
    pub const fn src(&self) -> &S {
        match self {
            BoundReached::Lower(e) => &e.src,
            BoundReached::Upper(e) => &e.src,
            BoundReached::Precision(e) => &e.src,
            BoundReached::Boolean(e) => &e.src,
        }
    }
    pub const fn best_match(&self) -> &D {
        match self {
            BoundReached::Lower(e) => &e.bound,
            BoundReached::Upper(e) => &e.bound,
            BoundReached::Precision(e) => &e.best_match,
            BoundReached::Boolean(e) => &e.best_match,
        }
    }
}
impl<S: PartialEq + Display, D: Display> Display for BoundReached<S, D> {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        match self {
            BoundReached::Lower(err) => err.fmt(f),
            BoundReached::Upper(err) => err.fmt(f),
            BoundReached::Precision(err) => err.fmt(f),
            BoundReached::Boolean(err) => err.fmt(f),
        }
    }
}
#[cfg(any(feature = "std", feature = "error_in_core"))]
impl<S: PartialEq + Display + Debug, D: Display + Debug> Error for BoundReached<S, D> {}
