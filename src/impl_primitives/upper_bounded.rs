use crate::{LosslessFrom, UpperBoundReached};
macro_rules! impl_upper_bounded {
    ($S: ident; $($D: ident),+; $V: expr; $F: expr) => {$(
        impl LosslessFrom<$S> for $D {
            type TError = UpperBoundReached<$S, $D>;
            fn lossless_from(v: $S) -> Result<Self, Self::TError> {
                if(v > ($D::MAX as $S)){
                    Err(Self::TError::new(v, $D::MIN))
                } else {
                    Ok(v as $D)
                }
            }
        }
        paste::paste!{
            #[test]
            #[cfg(test)]
            fn [<test_ $S _to_ $D>]() {
                for n in $V{
                    assert_eq!(Ok(n as $D), $D::lossless_from(n as $S));
                }
                for n in $F{
                    assert_eq!(Err(UpperBoundReached::<$S, $D>::new(n, $D::MIN)), $D::lossless_from(n as $S));
                }
            }
        }
    )+};
}
impl_upper_bounded!(u8; i8; [i16::from(u8::MIN), i16::from(i8::MAX)]; [u8::MAX]);
impl_upper_bounded!(u16; i8, i16, u8; [i16::from(u8::MIN), i16::from(i8::MAX)]; [u16::MAX]);
impl_upper_bounded!(u32; i8, i16, i32, u8, u16; [i16::from(u8::MIN), i16::from(i8::MAX)]; [u32::MAX]);
impl_upper_bounded!(u64; i8, i16, i32, i64, u8, u16, u32; [i16::from(u8::MIN), i16::from(i8::MAX)]; [u64::MAX]);
impl_upper_bounded!(u128; i8, i16, i32, i64, i128, u8, u16, u32, u64; [i16::from(u8::MIN), i16::from(i8::MAX)]; [u128::MAX]);
