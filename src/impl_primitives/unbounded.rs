use crate::{Infallible, LosslessFrom};
macro_rules! impl_unbounded {
    ($S: ident; $($D: ident),+; $N: expr) => {$(
        impl const LosslessFrom<$S> for $D {
            type TError = Infallible;
            fn lossless_from(v: $S) -> Result<Self, Self::TError> {
                Ok(v as $D)
            }
        }
        paste::paste!{
            #[cfg(test)]
            #[test]
            fn [<test_ $S _to_ $D>]() {
                for n in $N{
                    assert_eq!(Ok(n as $D), $D::lossless_from(n as $S));
                }
            }
        }
    )+};
}
impl_unbounded!(i8; i16, i32, i64, i128, f32, f64; [i8::MIN, 0, i8::MAX]);
impl_unbounded!(i16; i32, i64, i128, f32, f64; [i16::MIN, 0, i16::MAX]);
impl_unbounded!(i32; i64, i128, f64; [i32::MIN, 0, i32::MAX]);
impl_unbounded!(i64; i128; [i64::MIN, 0, i64::MAX]);

impl_unbounded!(u8; i16, i32, i64, i128, u16, u32, u64, u128, f32, f64; [u8::MIN, 0, u8::MAX]);
impl_unbounded!(u16; i32, i64, i128, u32, u64, u128, f32, f64; [u16::MIN, 0, u16::MAX]);
impl_unbounded!(u32; i64, i128, u64, u128, f64; [u32::MIN, 0, u32::MAX]);
impl_unbounded!(u64; i128, u128; [u64::MIN, 0, u64::MAX]);
impl_unbounded!(f32; f64; [f32::MIN, 0f32, f32::MAX]);
