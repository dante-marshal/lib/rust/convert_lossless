use crate::{LosslessFrom, LowerBoundReached};
macro_rules! impl_lower_bounded {
    ($S: ident; $($D: ident),+; $V: expr; $F: expr) => {$(
        impl LosslessFrom<$S> for $D {
            type TError = LowerBoundReached<$S, $D>;
            fn lossless_from(v: $S) -> Result<Self, Self::TError> {
                if(v < ($D::MIN as $S)){
                    Err(Self::TError::new(v, $D::MIN))
                } else {
                    Ok(v as $D)
                }
            }
        }
        paste::paste!{
            #[test]
            #[cfg(test)]
            fn [<test_ $S _to_ $D>]() {
                for n in $V{
                    assert_eq!(Ok(n as $D), $D::lossless_from(n as $S));
                }
                for n in $F{
                    assert_eq!(Err(LowerBoundReached::<$S, $D>::new(n, $D::MIN)), $D::lossless_from(n as $S));
                }
            }
        }
    )+};
}
impl_lower_bounded!(i8; u8, u16, u32, u64, u128; [u8::MIN, i8::MAX as u8]; [i8::MIN, -1i8]);
impl_lower_bounded!(i16; u16, u32, u64, u128; [u16::MIN, i16::MAX as u16]; [i16::MIN, -1i16]);
impl_lower_bounded!(i32; u32, u64, u128; [u32::MIN, i32::MAX as u32]; [i32::MIN, -1i32]);
impl_lower_bounded!(i64; u64, u128; [u64::MIN, i64::MAX as u64]; [i64::MIN, -1i64]);
impl_lower_bounded!(i128; u128; [u128::MIN, i128::MAX as u128]; [i128::MIN, -1i128]);
