#![cfg_attr(not(feature = "std"), no_std)]
#![deny(clippy::cast_lossless)]
#![deny(clippy::cast_possible_truncation)]
#![deny(clippy::cast_precision_loss)]
#![deny(clippy::cast_possible_wrap)]
#![feature(const_trait_impl)]
#![feature(const_default_impls)]
#[cfg(all(feature = "std", feature = "alloc"))]
compile_error!("feature \"std\" and feature \"alloc\" cannot be enabled at the same time");
#[cfg(all(feature = "std", feature = "error_in_core"))]
compile_error!("feature \"std\" and feature \"error_in_core\" cannot be enabled at the same time");
pub mod impl_primitives;
pub mod impl_single_item_containers;
pub mod impl_string;
pub mod traits;
pub use impl_primitives::*;
pub use impl_single_item_containers::*;
pub use impl_string::*;
pub use traits::*;
