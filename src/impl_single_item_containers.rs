mod errors;
#[cfg(any(feature = "std", feature = "alloc"))]
mod impl_vec;
pub mod markers {
    pub trait LosslessSingleItem {}
}
pub use errors::*;
